import { SESSION_INIT } from "../actions/sessionActions";

let defaultSession = {
  initialised: false
};

export default function reducer(state = defaultSession, action) {
  switch (action.type) {
    case SESSION_INIT: {
      return { ...state, initialised: true };
    }
  }
  return state;
}
