export const SESSION_INIT = 'SESSION.INIT';

export function initSession() {
  return {
    type: SESSION_INIT
  }
}
