import React from 'react';

import { connect } from 'react-redux';

import * as sessionActions from '../../actions/sessionActions'

@connect((store) => {
  return {
  }
})
class IndexScene extends React.Component {

  componentDidMount() {
    this.props.dispatch(sessionActions.initSession());
  }

  render() {
    console.log('the index scene');
    return (
      <div className="index-scene">
        <h1>Hello from index Scene</h1>
      </div>
    );
  }
}

export default IndexScene;
