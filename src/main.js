import React from "react";
import ReactDOM from "react-dom";

import { AppContainer } from 'react-hot-loader';
import { Provider } from 'react-redux';

import Root from './config/Root'
import store from './store/store';

import './main.scss'

const render = (Component) => {
  ReactDOM.render(
    <Provider store={store}>
      <AppContainer>
        <Component />
      </AppContainer>
    </Provider>,
    document.getElementById('root')
  )
}

render(Root);

if (module.hot) {
  module.hot.accept('./config/Root', () => {
    const newApp = require('./config/Root').default;
    render(newApp);
  });
}
