import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import IndexScene from '../scenes/index/index-scene';

const Root = () => {
  return (
    <Router>
      <Switch>
        <Route path="/" component={IndexScene} exact />
      </Switch>
    </Router>
  );
};

export default Root;

